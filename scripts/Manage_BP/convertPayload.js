/*
// read from existing workflow context 
var productInfo = $.context.productInfo; 
var productName = productInfo.productName; 
var productDescription = productInfo.productDescription;

// read contextual information
var taskDefinitionId = $.info.taskDefinitionId;

// read user task information
var lastUserTask1 = $.usertasks.usertask1.last;
var userTaskSubject = lastUserTask1.subject;
var userTaskProcessor = lastUserTask1.processor;
var userTaskCompletedAt = lastUserTask1.completedAt;

var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskProcessor + " at " + userTaskCompletedAt;

// create new node 'product'
var product = {
		productDetails: productName  + " " + productDescription,
		workflowStep: taskDefinitionId
};

// write 'product' node to workflow context
$.context.product = product;
*/
///*************Temp setting email id******************///
var email = "vijay.singh@spar.co.za";
$.context.emailId = email;
/////*****************************************************///
var data = $.context.data;
var BusinessPartner = data.code;
var OrganizationBPName1 = data.nameGroupDetail;
var SearchTerm1 = data.SearchTerm1;
var BusinessPartnerIsBlocked = false;
var BusinessPartnerCategory = "2";
var BusinessPartnerGrouping = data.group;
var BPIdentificationType_1 = "Z001";
var BPIdentificationType_2 = "Z002";
var BPIdentificationType_3 = "Z003";
var BPIdentificationNumber = "ABVKD93498";
////**********************Start of Address data**************/////

var complexNumberSAddress = data.address.complexNumberSAddress;
var streetNumberSAddress = data.address.streetNumberSAddress;
var streetNameSAddress = data.address.streetNameSAddress;
var subrubSAddress = data.address.subrubSAddress;
var citySAddress = data.address.citySAddress;
var postalCodeSAddress = data.address.postalCodeSAddress;
var provinceSAddres = data.address.provinceSAddres;
var countrySAddress = data.address.countrySAddress;
var PrfrdCommMediumType = "INT";
var StreetPrefixName = "";

var addressTypePAddress = data.address.addressTypePAddress;
var addressNumberPAddress = data.address.addressNumberPAddress;
var cityPAddress = data.address.cityPAddress;
var postalCodePAddress = data.addresspostalCodePAddress;
var provincePAddress = data.address.provincePAddress;
var countryPAddress = data.address.countryPAddress;
var AddressUsage = "XXDEFAULT";
//////*********************End of Address Data*****************/////

//////*********************Start of Contact Data*****************/////

///**********Creating Telephone Object-to_PhoneNumber***********///

var telephoneArray = data.contacts.telephoneData;
var to_PhoneNumber = [];
for (var iPhone = 0; iPhone < telephoneArray.length; iPhone++) {
	to_PhoneNumber[iPhone].IsDefaultPhoneNumber = telephoneArray[iPhone].primary;
	to_PhoneNumber[iPhone].PhoneNumber = telephoneArray[iPhone].telephoneNo;
	to_PhoneNumber[iPhone].PhoneNumberExtension = telephoneArray[iPhone].telephoneExtension;
	to_PhoneNumber[iPhone].InternationalPhoneNumber = telephoneArray[iPhone].telephoneDialingCode + " " + telephoneArray[iPhone].telephoneNo;
	to_PhoneNumber[iPhone].PhoneNumberType = telephoneArray[iPhone].idTelephoneTabContactType;
	to_PhoneNumber[iPhone].OrdinalNumber = iPhone;
}
///**********Creating Cellphone Object-to_PhoneNumber***********///
////------------------------------------------------------------////
var cellphoneArray = data.contacts.cellphoneData;
var totalLength = telephoneArray.length + cellphoneArray.lenght;
// var to_PhoneNumber = [];
for (; iPhone < totalLength; iPhone++) {
	to_PhoneNumber[iPhone].IsDefaultPhoneNumber = telephoneArray[iPhone].primary;
	to_PhoneNumber[iPhone].PhoneNumber = telephoneArray[iPhone].cellphoneNo;
	to_PhoneNumber[iPhone].InternationalPhoneNumber = telephoneArray[iPhone].cellphoneDialingCode + " " + telephoneArray[iPhone].cellphoneNo;
	to_PhoneNumber[iPhone].PhoneNumberType = telephoneArray[iPhone].cellphoneContactType;
	to_PhoneNumber[iPhone].OrdinalNumber = iPhone;
}
///**********Creating Fax Object-to_FaxNumber***********///
////------------------------------------------------------------////
var faxArray = data.contacts.faxData;
var to_FaxNumber = [];
for (var iFax = 0; iFax < faxArray.length; iFax++) {
	to_FaxNumber[iFax].IsDefaultPhoneNumber = faxArray[iFax].primary;
	to_FaxNumber[iFax].FaxNumber = faxArray[iFax].faxNo;
	to_FaxNumber[iFax].InternationalFaxNumber = faxArray[iFax].faxDialingCode + " " + faxArray[iFax].faxNo;
	to_FaxNumber[iFax].OrdinalNumber = iFax;
}
///**********Creating Eail Object-to_EmailAddress***********///
////------------------------------------------------------------////
var emailArray = data.contacts.emailData;
var to_EmailAddress = [];
for (var iEmail = 0; iEmail < faxArray.length; iEmail++) {
	to_EmailAddress[iEmail].IsDefaultPhoneNumber = emailArray[iEmail].primary;
	to_EmailAddress[iEmail].EmailAddress = emailArray[iEmail].EmailAddress;
	to_EmailAddress[iEmail].SearchEmailAddress = emailArray[iEmail].EmailAddress;
	to_EmailAddress[iEmail].OrdinalNumber = iEmail;
}
//////*********************End of Contact Data*****************/////
////************************************************************////

///**************Business Partner Role - to_BusinessPartnerRole*************////
////_______________________________________________________________________/////
var to_BusinessPartnerRole = [{
	BusinessPartner: BusinessPartner,
	BusinessPartnerRole: "BPSITE"
}, {
	BusinessPartner: BusinessPartner,
	BusinessPartnerRole: "ZFLCU0"
}, {
	BusinessPartner: BusinessPartner,
	BusinessPartnerRole: "ZFLCU1"
}, {
	BusinessPartner: BusinessPartner,
	BusinessPartnerRole: "ZFLVN0"
}, {
	BusinessPartner: BusinessPartner,
	BusinessPartnerRole: "ZFLVN1"
}, {
	BusinessPartner: BusinessPartner,
	BusinessPartnerRole: "FLCU00"
}, {
	BusinessPartner: BusinessPartner,
	BusinessPartnerRole: "FLVN01"
}, {
	BusinessPartner: "80314",
	BusinessPartnerRole: "FLCU01"
}, {
	BusinessPartner: BusinessPartner,
	BusinessPartnerRole: "FLVN00"
}, {
	BusinessPartner: BusinessPartner,
	BusinessPartnerRole: "UKM000"
}];
//////*********************Creation of Request Object*****************/////

var request = {
	BusinessPartner: BusinessPartner,
	Customer: BusinessPartner,
	Supplier: BusinessPartner,
	OrganizationBPName1: OrganizationBPName1,
	OrganizationBPName4: OrganizationBPName1,
	SearchTerm1: SearchTerm1,
	BusinessPartnerIsBlocked: BusinessPartnerIsBlocked,
	BusinessPartnerCategory: BusinessPartnerCategory,
	BusinessPartnerGrouping: BusinessPartnerGrouping,
	to_BuPaIdentification: [{
		BusinessPartner: BusinessPartner,
		BPIdentificationType: BPIdentificationType_1,
		BPIdentificationNumber: BPIdentificationNumber,
		Country: countrySAddress,
		Region: provinceSAddres
	}, {
		BusinessPartner: BusinessPartner,
		BPIdentificationType: BPIdentificationType_2,
		BPIdentificationNumber: BPIdentificationNumber,
		Country: countrySAddress,
		Region: provinceSAddres
	}, {
		BusinessPartner: BusinessPartner,
		BPIdentificationType: BPIdentificationType_3,
		BPIdentificationNumber: BPIdentificationNumber,
		Country: countrySAddress,
		Region: provinceSAddres
	}],
	to_BusinessPartnerAddress: [{
		BusinessPartner: BusinessPartner,
		CityName: citySAddress,
		Country: countrySAddress,
		District: subrubSAddress,
		HouseNumber: complexNumberSAddress,
		// Language:, 
		PostalCode: postalCodeSAddress,
		PrfrdCommMediumType: PrfrdCommMediumType,
		Region: provinceSAddres,
		StreetName: streetNameSAddress,
		StreetPrefixName: StreetPrefixName,
		////**********Postal Address*******///
		DeliveryServiceNumber: addressNumberPAddress,
		DeliveryServiceTypeCode: addressTypePAddress,
		POBoxPostalCode: postalCodePAddress,
		POBoxDeviatingCityName: cityPAddress,
		POBoxDeviatingRegion: provincePAddress,
		POBoxDeviatingCountry: countryPAddress,
		to_AddressUsage: [{
			BusinessPartner: BusinessPartner,
			//ValidityEndDate:"9999-01-01T00:00:00Z",
			AddressUsage: AddressUsage
				//ValidityStartDate":"2018-09-21T15:20:15Z"
		}],
		to_PhoneNumber: to_PhoneNumber,
		to_FaxNumber: to_FaxNumber,
		to_EmailAddress: to_EmailAddress
			// to_EmailAddress :
			// IsDefaultEmailAddress : true,
			// EmailAddress : 
			// manager @knowlesops.co.za ,
			// SearchEmailAddress : ,
			// OrdinalNumber : 

	}],
	to_BusinessPartnerRole: to_BusinessPartnerRole
};
$.context.request = request;
// $.context.request.BusinessPartner = $.context.data.code;
// $.context.request.OrganizationBPName1 = $.context.data.nameGroupDetail;