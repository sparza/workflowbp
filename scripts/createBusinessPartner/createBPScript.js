			var xcsrfToken = "";
			var wfParameter = $.context.request;

			// wfParameter = {
			// 	"BusinessPartner": "82125",
			// 	"Customer": "",
			// 	"Supplier": "",
			// 	"AcademicTitle": "",
			// 	"AuthorizationGroup": "",
			// 	"BusinessPartnerCategory": "2",
			// 	"BusinessPartnerFullName": "ALPHA SUPERSPAR",
			// 	"BusinessPartnerGrouping": "ZSTV",
			// 	"BusinessPartnerName": "ALPHA SUPERSPAR",
			// 	"BusinessPartnerUUID": "00505686-6cf6-1eda-93cd-5b4587a480c3",
			// 	"CorrespondenceLanguage": "",
			// 	"CreatedByUser": "SINGHVIJ",
			// 	"CreationDate": "\/Date(1581552000000)\/",
			// 	"CreationTime": "PT16H15M36S",
			// 	"FirstName": "",
			// 	"FormOfAddress": "",
			// 	"Industry": "",
			// 	"InternationalLocationNumber1": "6001008",
			// 	"InternationalLocationNumber2": "40000",
			// 	"IsFemale": false,
			// 	"IsMale": false,
			// 	"IsNaturalPerson": "",
			// 	"IsSexUnknown": false,
			// 	"Language": "",
			// 	"LastChangeDate": null,
			// 	"LastChangeTime": "PT00H00M00S",
			// 	"LastChangedByUser": "",
			// 	"LastName": "",
			// 	"LegalForm": "",
			// 	"OrganizationBPName1": "ALPHA-UPDATT",
			// 	"OrganizationBPName2": "",
			// 	"OrganizationBPName3": "",
			// 	"OrganizationBPName4": "",
			// 	"OrganizationFoundationDate": null,
			// 	"OrganizationLiquidationDate": null,
			// 	"SearchTerm1": "ALPHA",
			// 	"AdditionalLastName": "",
			// 	"BirthDate": null,
			// 	"BusinessPartnerIsBlocked": false,
			// 	"BusinessPartnerType": "",
			// 	"ETag": "SINGHVIJ20200213161536",
			// 	"GroupBusinessPartnerName1": "",
			// 	"GroupBusinessPartnerName2": "",
			// 	"IndependentAddressID": "",
			// 	"InternationalLocationNumber3": "0",
			// 	"MiddleName": "",
			// 	"NameCountry": "",
			// 	"NameFormat": "",
			// 	"PersonFullName": "",
			// 	"PersonNumber": "",
			// 	"IsMarkedForArchiving": false
			// };
			$.ajax({
				url: "/FS1/API_BUSINESS_PARTNER?$format=json",
				type: "GET",
				headers: {
					"X-CSRF-Token": "Fetch"
				},
				success: function (result, xhr, data) {
					xcsrfToken = data.getResponseHeader("X-CSRF-Token");
					$.ajax({
						url: "/FS1/API_BUSINESS_PARTNER/A_BusinessPartner",
						method: "POST",
						headers: {
							"X-CSRF-Token": xcsrfToken,
							"Content-Type": "application/json"
						},
						data: JSON.stringify(wfParameter),
						success: function (result, xhr, data) {
							// sap.m.MessageToast.show("Work Flow started successfuly");
						},
						error: function (jqXHR, status, error) {
							// sap.m.MessageToast.show("Filed to start the workflow");
						}
					});
				},
				error: function (jqXHR, status, error) {
					sap.m.MessageToast.show("Filed to fetch the XSCRF Token");
				}
			});
			/*
			// read from existing workflow context 
			var productInfo = $.context.productInfo; 
			var productName = productInfo.productName; 
			var productDescription = productInfo.productDescription;

			// read contextual information
			var taskDefinitionId = $.info.taskDefinitionId;

			// read user task information
			var lastUserTask1 = $.usertasks.usertask1.last;
			var userTaskSubject = lastUserTask1.subject;
			var userTaskProcessor = lastUserTask1.processor;
			var userTaskCompletedAt = lastUserTask1.completedAt;

			var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskProcessor + " at " + userTaskCompletedAt;

			// create new node 'product'
			var product = {
					productDetails: productName  + " " + productDescription,
					workflowStep: taskDefinitionId
			};

			// write 'product' node to workflow context
			$.context.product = product;
			*/