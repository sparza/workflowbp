{
	"contents": {
		"a528c89b-72ab-47b2-8095-e76678b56bf6": {
			"classDefinition": "com.sap.bpm.wfs.Model",
			"id": "manage_bp",
			"subject": "Manage_BP",
			"name": "Manage_BP",
			"documentation": "",
			"lastIds": "689b8833-adb9-45d6-93be-a618abdc28d4",
			"events": {
				"720f397b-9b2c-4f64-8a34-d1c4f4d97d4c": {
					"name": "StartEvent1"
				},
				"63a8603e-5533-4694-bd96-976dd05dcd6e": {
					"name": "EndEvent1"
				},
				"0974929d-1782-4761-ada4-ee6ba312e9be": {
					"name": "BoundaryTimerEvent1"
				},
				"037127a8-ab1b-4f54-a2ad-0927c1be0981": {
					"name": "BoundaryTimerEvent2"
				}
			},
			"activities": {
				"990b9697-1123-4ca5-9eca-c322d358514d": {
					"name": "ExclusiveGateway1"
				},
				"744b65ea-ad7a-49c7-8170-4a35b5f92626": {
					"name": "Create_BP"
				},
				"81731999-2c7a-40a1-a52a-5cc238238605": {
					"name": "ExclusiveGateway2"
				},
				"22bdf65f-e2b1-4f19-915c-703616a49c95": {
					"name": "Update_BP"
				},
				"4dd068e3-50bf-4907-8e8d-189c79eb86b3": {
					"name": "Get_BP"
				},
				"c07c5e31-de33-40f3-b562-3e304098c6b0": {
					"name": "ExclusiveGateway4"
				},
				"14981457-9321-4ec4-978e-b55338af7770": {
					"name": "Create_Approval1"
				},
				"caf3a21e-65a5-459e-b900-9d8da21635ee": {
					"name": "GetOrg_BP"
				},
				"556699ef-229f-4a73-8149-76af5e6bbec0": {
					"name": "Update_Approval"
				},
				"5d04e883-64e6-43f0-bf85-0553f6e9c8ec": {
					"name": "MailTask_CreateBP"
				},
				"e7b22343-9350-40db-bbec-6d251a89b2a7": {
					"name": "MailTask_UpdateBP"
				},
				"436e1a00-db01-4b9b-beae-5a3fec66f632": {
					"name": "ExclusiveGateway5"
				},
				"9168f39c-ddba-4784-bf4a-38e865fedd4d": {
					"name": "convertingPayload"
				}
			},
			"sequenceFlows": {
				"f2f79b8d-b764-44b7-a8ea-42df64925dae": {
					"name": "SequenceFlow4"
				},
				"289210bd-e892-4a3b-bd89-6d005cd69d28": {
					"name": "To Create BP"
				},
				"66866316-d68d-4714-ab3e-4bae1484fb15": {
					"name": "SequenceFlow16"
				},
				"d165c590-3658-45d0-bdeb-53cb5da0fde7": {
					"name": "SequenceFlow17"
				},
				"533a7b5a-d2e2-495d-86b8-12d93f25ac9c": {
					"name": "SequenceFlow18"
				},
				"d7cacca6-c3c7-4879-b67b-5bd8939ce5c4": {
					"name": "SequenceFlow20"
				},
				"5bd266bf-e39a-4d72-bbde-2cd056adcf7b": {
					"name": "SequenceFlow22"
				},
				"cd354ce3-b5fc-4ce3-9d19-1c6e726904af": {
					"name": "SequenceFlow23"
				},
				"42fde8cb-f41b-4a73-a1bf-b470fce59ae9": {
					"name": "SequenceFlow24"
				},
				"100ba38c-9bde-4ec0-9f8f-f5a357add8a3": {
					"name": "SequenceFlow27"
				},
				"1459b707-c32d-48d6-952b-18bc23bb6fde": {
					"name": "SequenceFlow28"
				},
				"f2f3e0ec-e8b1-4278-be78-412792743b9d": {
					"name": "SequenceFlow30"
				},
				"55de8f0f-415d-4ed3-81c0-adb99762554e": {
					"name": "SequenceFlow33"
				},
				"9be491f4-ae62-43eb-93ea-4c1a8996faca": {
					"name": "SequenceFlow34"
				},
				"30a83deb-29e0-40cc-9486-c15b0ba6f417": {
					"name": "SequenceFlow36"
				},
				"9b721ade-6c63-43e7-bbe3-db6bf29fc42f": {
					"name": "SequenceFlow41"
				},
				"2c7bdbd0-9874-42f1-9f55-78cbdfa530db": {
					"name": "SequenceFlow42"
				},
				"2100776b-6bf6-4930-8ffc-4ee3018669ab": {
					"name": "SequenceFlow43"
				},
				"059bd362-3a1c-43c5-bda1-d6c2d303904a": {
					"name": "SequenceFlow44"
				}
			},
			"diagrams": {
				"3cdfee8e-376b-44ff-adbe-ab69f8a779a7": {}
			}
		},
		"720f397b-9b2c-4f64-8a34-d1c4f4d97d4c": {
			"classDefinition": "com.sap.bpm.wfs.StartEvent",
			"id": "startevent1",
			"name": "StartEvent1",
			"sampleContextRefs": {
				"08f67112-7061-4e22-b567-56bf93e17a80": {}
			}
		},
		"63a8603e-5533-4694-bd96-976dd05dcd6e": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent1",
			"name": "EndEvent1"
		},
		"0974929d-1782-4761-ada4-ee6ba312e9be": {
			"classDefinition": "com.sap.bpm.wfs.BoundaryEvent",
			"isCanceling": true,
			"id": "boundarytimerevent1",
			"name": "BoundaryTimerEvent1",
			"attachedToRef": "556699ef-229f-4a73-8149-76af5e6bbec0",
			"eventDefinitions": {
				"c46769ea-64dd-4a60-a127-b2e0824232f6": {}
			}
		},
		"037127a8-ab1b-4f54-a2ad-0927c1be0981": {
			"classDefinition": "com.sap.bpm.wfs.BoundaryEvent",
			"isCanceling": true,
			"id": "boundarytimerevent2",
			"name": "BoundaryTimerEvent2",
			"attachedToRef": "14981457-9321-4ec4-978e-b55338af7770",
			"eventDefinitions": {
				"8c2f0bbf-233a-4373-8443-371ef0ec32c6": {}
			}
		},
		"990b9697-1123-4ca5-9eca-c322d358514d": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway1",
			"name": "ExclusiveGateway1",
			"default": "2100776b-6bf6-4930-8ffc-4ee3018669ab"
		},
		"744b65ea-ad7a-49c7-8170-4a35b5f92626": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "FS1",
			"path": "/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner",
			"httpMethod": "POST",
			"xsrfPath": "/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/StoreFormatSet",
			"requestVariable": "${context.data}",
			"responseVariable": "${context.response}",
			"id": "servicetask1",
			"name": "Create_BP"
		},
		"81731999-2c7a-40a1-a52a-5cc238238605": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway2",
			"name": "ExclusiveGateway2",
			"default": "100ba38c-9bde-4ec0-9f8f-f5a357add8a3"
		},
		"22bdf65f-e2b1-4f19-915c-703616a49c95": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "FS1",
			"path": "/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner('${context.request.BusinessPartner}')",
			"httpMethod": "PUT",
			"xsrfPath": "/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/StoreFormatSet",
			"requestVariable": "${context.request}",
			"responseVariable": "${context.response}",
			"id": "servicetask2",
			"name": "Update_BP"
		},
		"4dd068e3-50bf-4907-8e8d-189c79eb86b3": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "FS1",
			"path": "/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner(BusinessPartner='${context.request.BusinessPartner}')?$format=json&$expand=to_BuPaIdentification,to_BusinessPartnerAddress,to_BusinessPartnerAddress/to_EmailAddress,to_BusinessPartnerAddress/to_FaxNumber,to_BusinessPartnerAddress/to_MobilePhoneNumber,to_BusinessPartnerAddress/to_PhoneNumber,to_BusinessPartnerAddress/to_URLAddress,to_BusinessPartnerBank,to_BusinessPartnerContact,to_BusinessPartnerContact/to_ContactAddress,to_BusinessPartnerContact/to_ContactRelationship,to_BusinessPartnerRole,to_BusinessPartnerTax,to_Customer,to_Customer/to_CustomerCompany,to_Customer/to_CustomerCompany/to_CustomerDunning,to_Customer/to_CustomerCompany/to_WithHoldingTax,to_Customer/to_CustomerSalesArea,to_Customer/to_CustomerSalesArea/to_PartnerFunction,to_Customer/to_CustomerSalesArea/to_SalesAreaTax,to_Supplier,to_Supplier/to_SupplierCompany,to_Supplier/to_SupplierCompany/to_SupplierDunning,to_Supplier/to_SupplierCompany/to_SupplierWithHoldingTax,to_Supplier/to_SupplierPurchasingOrg,to_Supplier/to_SupplierPurchasingOrg/to_PartnerFunction",
			"httpMethod": "GET",
			"responseVariable": "${context.response}",
			"id": "servicetask3",
			"name": "Get_BP"
		},
		"c07c5e31-de33-40f3-b562-3e304098c6b0": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway4",
			"name": "ExclusiveGateway4"
		},
		"14981457-9321-4ec4-978e-b55338af7770": {
			"classDefinition": "com.sap.bpm.wfs.UserTask",
			"subject": "Approval for Create Business Partner",
			"priority": "MEDIUM",
			"isHiddenInLogForParticipant": false,
			"userInterface": "sapui5://html5apps/bpmformplayer/com.sap.bpm.wus.form.player",
			"recipientUsers": "P1942915070",
			"formReference": "/forms/Manage_BP/CreateApproval.form",
			"userInterfaceParams": [{
				"key": "formId",
				"value": "createapproval"
			}, {
				"key": "formRevision",
				"value": "1.0"
			}],
			"customAttributes": [],
			"id": "usertask1",
			"name": "Create_Approval1"
		},
		"caf3a21e-65a5-459e-b900-9d8da21635ee": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "FS1",
			"path": "/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner(BusinessPartner='${context.request.BusinessPartner}')?$format=json&$expand=to_BuPaIdentification,to_BusinessPartnerAddress,to_BusinessPartnerAddress/to_EmailAddress,to_BusinessPartnerAddress/to_FaxNumber,to_BusinessPartnerAddress/to_MobilePhoneNumber,to_BusinessPartnerAddress/to_PhoneNumber,to_BusinessPartnerAddress/to_URLAddress,to_BusinessPartnerBank,to_BusinessPartnerContact,to_BusinessPartnerContact/to_ContactAddress,to_BusinessPartnerContact/to_ContactRelationship,to_BusinessPartnerRole,to_BusinessPartnerTax,to_Customer,to_Customer/to_CustomerCompany,to_Customer/to_CustomerCompany/to_CustomerDunning,to_Customer/to_CustomerCompany/to_WithHoldingTax,to_Customer/to_CustomerSalesArea,to_Customer/to_CustomerSalesArea/to_PartnerFunction,to_Customer/to_CustomerSalesArea/to_SalesAreaTax,to_Supplier,to_Supplier/to_SupplierCompany,to_Supplier/to_SupplierCompany/to_SupplierDunning,to_Supplier/to_SupplierCompany/to_SupplierWithHoldingTax,to_Supplier/to_SupplierPurchasingOrg,to_Supplier/to_SupplierPurchasingOrg/to_PartnerFunction",
			"httpMethod": "GET",
			"responseVariable": "${context.orgbp}",
			"id": "servicetask4",
			"name": "GetOrg_BP"
		},
		"556699ef-229f-4a73-8149-76af5e6bbec0": {
			"classDefinition": "com.sap.bpm.wfs.UserTask",
			"subject": "Approval for Update Business Partner",
			"priority": "MEDIUM",
			"isHiddenInLogForParticipant": false,
			"userInterface": "sapui5://html5apps/bpmformplayer/com.sap.bpm.wus.form.player",
			"recipientUsers": "P1942915070",
			"formReference": "/forms/Manage_BP/ChangeApproval.form",
			"userInterfaceParams": [{
				"key": "formId",
				"value": "changeapproval"
			}, {
				"key": "formRevision",
				"value": "1.0"
			}],
			"id": "usertask2",
			"name": "Update_Approval"
		},
		"5d04e883-64e6-43f0-bf85-0553f6e9c8ec": {
			"classDefinition": "com.sap.bpm.wfs.MailTask",
			"id": "mailtask1",
			"name": "MailTask_CreateBP",
			"mailDefinitionRef": "5bd67148-54ff-414e-b276-364533f433dd"
		},
		"e7b22343-9350-40db-bbec-6d251a89b2a7": {
			"classDefinition": "com.sap.bpm.wfs.MailTask",
			"id": "mailtask2",
			"name": "MailTask_UpdateBP",
			"mailDefinitionRef": "f410760b-87fb-42bd-8936-b5f27924d154"
		},
		"436e1a00-db01-4b9b-beae-5a3fec66f632": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway5",
			"name": "ExclusiveGateway5",
			"default": "9b721ade-6c63-43e7-bbe3-db6bf29fc42f"
		},
		"9168f39c-ddba-4784-bf4a-38e865fedd4d": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/Manage_BP/convertPayload.js",
			"id": "scripttask2",
			"name": "convertingPayload"
		},
		"f2f79b8d-b764-44b7-a8ea-42df64925dae": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.actionUpdate == true}",
			"id": "sequenceflow4",
			"name": "SequenceFlow4",
			"sourceRef": "990b9697-1123-4ca5-9eca-c322d358514d",
			"targetRef": "81731999-2c7a-40a1-a52a-5cc238238605"
		},
		"289210bd-e892-4a3b-bd89-6d005cd69d28": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.actionCreate == true}",
			"id": "sequenceflow11",
			"name": "To Create BP",
			"sourceRef": "990b9697-1123-4ca5-9eca-c322d358514d",
			"targetRef": "9168f39c-ddba-4784-bf4a-38e865fedd4d"
		},
		"66866316-d68d-4714-ab3e-4bae1484fb15": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow16",
			"name": "SequenceFlow16",
			"sourceRef": "caf3a21e-65a5-459e-b900-9d8da21635ee",
			"targetRef": "556699ef-229f-4a73-8149-76af5e6bbec0"
		},
		"d165c590-3658-45d0-bdeb-53cb5da0fde7": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow17",
			"name": "SequenceFlow17",
			"sourceRef": "556699ef-229f-4a73-8149-76af5e6bbec0",
			"targetRef": "22bdf65f-e2b1-4f19-915c-703616a49c95"
		},
		"533a7b5a-d2e2-495d-86b8-12d93f25ac9c": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow18",
			"name": "SequenceFlow18",
			"sourceRef": "c07c5e31-de33-40f3-b562-3e304098c6b0",
			"targetRef": "63a8603e-5533-4694-bd96-976dd05dcd6e"
		},
		"d7cacca6-c3c7-4879-b67b-5bd8939ce5c4": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow20",
			"name": "SequenceFlow20",
			"sourceRef": "5d04e883-64e6-43f0-bf85-0553f6e9c8ec",
			"targetRef": "14981457-9321-4ec4-978e-b55338af7770"
		},
		"5bd266bf-e39a-4d72-bbde-2cd056adcf7b": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow22",
			"name": "SequenceFlow22",
			"sourceRef": "e7b22343-9350-40db-bbec-6d251a89b2a7",
			"targetRef": "556699ef-229f-4a73-8149-76af5e6bbec0"
		},
		"cd354ce3-b5fc-4ce3-9d19-1c6e726904af": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow23",
			"name": "SequenceFlow23",
			"sourceRef": "0974929d-1782-4761-ada4-ee6ba312e9be",
			"targetRef": "e7b22343-9350-40db-bbec-6d251a89b2a7"
		},
		"42fde8cb-f41b-4a73-a1bf-b470fce59ae9": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow24",
			"name": "SequenceFlow24",
			"sourceRef": "037127a8-ab1b-4f54-a2ad-0927c1be0981",
			"targetRef": "5d04e883-64e6-43f0-bf85-0553f6e9c8ec"
		},
		"100ba38c-9bde-4ec0-9f8f-f5a357add8a3": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow27",
			"name": "SequenceFlow27",
			"sourceRef": "81731999-2c7a-40a1-a52a-5cc238238605",
			"targetRef": "caf3a21e-65a5-459e-b900-9d8da21635ee"
		},
		"1459b707-c32d-48d6-952b-18bc23bb6fde": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow28",
			"name": "SequenceFlow28",
			"sourceRef": "22bdf65f-e2b1-4f19-915c-703616a49c95",
			"targetRef": "c07c5e31-de33-40f3-b562-3e304098c6b0"
		},
		"f2f3e0ec-e8b1-4278-be78-412792743b9d": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${usertasks.usertask1.last.decision == 'reject'}",
			"id": "sequenceflow30",
			"name": "SequenceFlow30",
			"sourceRef": "436e1a00-db01-4b9b-beae-5a3fec66f632",
			"targetRef": "63a8603e-5533-4694-bd96-976dd05dcd6e"
		},
		"55de8f0f-415d-4ed3-81c0-adb99762554e": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow33",
			"name": "SequenceFlow33",
			"sourceRef": "14981457-9321-4ec4-978e-b55338af7770",
			"targetRef": "436e1a00-db01-4b9b-beae-5a3fec66f632"
		},
		"9be491f4-ae62-43eb-93ea-4c1a8996faca": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow34",
			"name": "SequenceFlow34",
			"sourceRef": "9168f39c-ddba-4784-bf4a-38e865fedd4d",
			"targetRef": "14981457-9321-4ec4-978e-b55338af7770"
		},
		"30a83deb-29e0-40cc-9486-c15b0ba6f417": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow36",
			"name": "SequenceFlow36",
			"sourceRef": "720f397b-9b2c-4f64-8a34-d1c4f4d97d4c",
			"targetRef": "990b9697-1123-4ca5-9eca-c322d358514d"
		},
		"9b721ade-6c63-43e7-bbe3-db6bf29fc42f": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow41",
			"name": "SequenceFlow41",
			"sourceRef": "436e1a00-db01-4b9b-beae-5a3fec66f632",
			"targetRef": "744b65ea-ad7a-49c7-8170-4a35b5f92626"
		},
		"2c7bdbd0-9874-42f1-9f55-78cbdfa530db": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow42",
			"name": "SequenceFlow42",
			"sourceRef": "744b65ea-ad7a-49c7-8170-4a35b5f92626",
			"targetRef": "c07c5e31-de33-40f3-b562-3e304098c6b0"
		},
		"2100776b-6bf6-4930-8ffc-4ee3018669ab": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow43",
			"name": "SequenceFlow43",
			"sourceRef": "990b9697-1123-4ca5-9eca-c322d358514d",
			"targetRef": "4dd068e3-50bf-4907-8e8d-189c79eb86b3"
		},
		"059bd362-3a1c-43c5-bda1-d6c2d303904a": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow44",
			"name": "SequenceFlow44",
			"sourceRef": "4dd068e3-50bf-4907-8e8d-189c79eb86b3",
			"targetRef": "63a8603e-5533-4694-bd96-976dd05dcd6e"
		},
		"3cdfee8e-376b-44ff-adbe-ab69f8a779a7": {
			"classDefinition": "com.sap.bpm.wfs.ui.Diagram",
			"symbols": {
				"1d602b09-19c8-4ff0-bc04-0a30c75784fc": {},
				"e04cb057-e5a1-44ec-8072-f256bebc46e1": {},
				"2c2b5b29-e733-40db-b919-521c99d2c798": {},
				"264a01c9-8168-4a5d-8e75-0e012952955a": {},
				"4ef6f2c7-95f5-44d5-8f2a-cb36b8934ba6": {},
				"715d9148-5a20-430d-be1a-ffb2508e2994": {},
				"7b5e8f03-9e60-473c-8817-298bfe837aae": {},
				"f5a25c1d-2539-4781-99b3-9f990f55494f": {},
				"d5fbfa6f-1551-49ea-94a8-520191cdb02a": {},
				"933de791-e8bc-404c-8a29-93a74d35fea2": {},
				"4526e92a-7e2a-4857-b4cb-3a6e20e52e0b": {},
				"56a945c7-1231-4918-ae37-772f50348aab": {},
				"4ebb0d6c-56a1-46a5-b5de-863c9eb6c062": {},
				"bb21ef78-e447-416c-b1ce-c87afc87c343": {},
				"d0cf69c1-b5e4-4192-a880-86f71f80aaf5": {},
				"3030c441-3a52-4207-b6cf-142fa6b380a6": {},
				"41d86f94-7e2f-4af3-b092-f87c14900b53": {},
				"2c427009-acf1-45a9-a759-818f3f9632ac": {},
				"dc5937d2-5736-4a47-b07c-708b773cfe25": {},
				"e03d5578-aa2b-492e-b335-dd30e4c84ff5": {},
				"b829c315-430e-415a-a4da-d156acaebb43": {},
				"d46048dc-6ac8-4fce-8eb1-dcf36acd96d1": {},
				"00fdaf34-0bd4-46b1-9799-ff79d96c8dc7": {},
				"8f3c62c3-c1aa-4183-9f07-c47fc89f7caf": {},
				"0df81231-921d-4023-a050-09aca5902f75": {},
				"afdce8da-5b11-4c92-846b-a34fbefdafa1": {},
				"a9b5c216-a7c8-4583-8c19-85b6d57be44c": {},
				"44c40ec9-f716-4337-b542-d6946f1ce74b": {},
				"b9cc4d7f-dbfb-4274-842f-828fcb4a1db9": {},
				"44050e9c-0b12-4e2e-a57f-02fe652bac32": {},
				"94512af6-318a-42cd-a592-28bf38431c7b": {},
				"d7e046d0-83fb-4fb7-a88f-8abf6288cf8f": {},
				"6b1f7d34-bac3-4650-9e70-ac3928fe9a25": {},
				"ec632b9c-b6fd-4cd3-bf70-a5e3582f77c9": {}
			}
		},
		"08f67112-7061-4e22-b567-56bf93e17a80": {
			"classDefinition": "com.sap.bpm.wfs.SampleContext",
			"reference": "/sample-data/BusinessPartner_Flow/initworkingBP.json",
			"id": "default-start-context"
		},
		"c46769ea-64dd-4a60-a127-b2e0824232f6": {
			"classDefinition": "com.sap.bpm.wfs.TimerEventDefinition",
			"timeDuration": "PT5M",
			"id": "timereventdefinition3"
		},
		"8c2f0bbf-233a-4373-8443-371ef0ec32c6": {
			"classDefinition": "com.sap.bpm.wfs.TimerEventDefinition",
			"timeDuration": "PT5M",
			"id": "timereventdefinition4"
		},
		"1d602b09-19c8-4ff0-bc04-0a30c75784fc": {
			"classDefinition": "com.sap.bpm.wfs.ui.StartEventSymbol",
			"x": 29,
			"y": 135.49999940395355,
			"width": 32,
			"height": 32,
			"object": "720f397b-9b2c-4f64-8a34-d1c4f4d97d4c"
		},
		"e04cb057-e5a1-44ec-8072-f256bebc46e1": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 1271.9999928474426,
			"y": 235.99999940395355,
			"width": 35,
			"height": 35,
			"object": "63a8603e-5533-4694-bd96-976dd05dcd6e"
		},
		"2c2b5b29-e733-40db-b919-521c99d2c798": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 209,
			"y": 130.49999940395355,
			"object": "990b9697-1123-4ca5-9eca-c322d358514d"
		},
		"264a01c9-8168-4a5d-8e75-0e012952955a": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 976.9999976158142,
			"y": 138.9999988079071,
			"width": 100,
			"height": 60,
			"object": "744b65ea-ad7a-49c7-8170-4a35b5f92626"
		},
		"4ef6f2c7-95f5-44d5-8f2a-cb36b8934ba6": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 292.9999988079071,
			"y": 63.49999940395355,
			"object": "81731999-2c7a-40a1-a52a-5cc238238605"
		},
		"715d9148-5a20-430d-be1a-ffb2508e2994": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "230,130.99999940395355 230,84.5 292.9999988079071,84.49999940395355",
			"sourceSymbol": "2c2b5b29-e733-40db-b919-521c99d2c798",
			"targetSymbol": "4ef6f2c7-95f5-44d5-8f2a-cb36b8934ba6",
			"object": "f2f79b8d-b764-44b7-a8ea-42df64925dae"
		},
		"7b5e8f03-9e60-473c-8817-298bfe837aae": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 938.9999976158142,
			"y": 18,
			"width": 100,
			"height": 60,
			"object": "22bdf65f-e2b1-4f19-915c-703616a49c95"
		},
		"f5a25c1d-2539-4781-99b3-9f990f55494f": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 520.9999988079071,
			"y": 374.49999940395355,
			"width": 100,
			"height": 60,
			"object": "4dd068e3-50bf-4907-8e8d-189c79eb86b3"
		},
		"d5fbfa6f-1551-49ea-94a8-520191cdb02a": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "231.9999988079071,147.49999940395355 286,147.5 286,236 371,236",
			"sourceSymbol": "2c2b5b29-e733-40db-b919-521c99d2c798",
			"targetSymbol": "44c40ec9-f716-4337-b542-d6946f1ce74b",
			"object": "289210bd-e892-4a3b-bd89-6d005cd69d28"
		},
		"933de791-e8bc-404c-8a29-93a74d35fea2": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 1149.9999940395355,
			"y": 117.9999988079071,
			"object": "c07c5e31-de33-40f3-b562-3e304098c6b0"
		},
		"4526e92a-7e2a-4857-b4cb-3a6e20e52e0b": {
			"classDefinition": "com.sap.bpm.wfs.ui.UserTaskSymbol",
			"x": 557.9999964237213,
			"y": 129.99999910593033,
			"width": 100,
			"height": 60,
			"object": "14981457-9321-4ec4-978e-b55338af7770",
			"symbols": {
				"de005a46-4df5-4a5a-ab29-06c8241dd175": {}
			}
		},
		"56a945c7-1231-4918-ae37-772f50348aab": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 420.9999964237213,
			"y": 2.980232238769531e-7,
			"width": 100,
			"height": 60,
			"object": "caf3a21e-65a5-459e-b900-9d8da21635ee"
		},
		"4ebb0d6c-56a1-46a5-b5de-863c9eb6c062": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "520.9999964237213,30.000000298023224 660.75,30 660.75,53 802.9999952316284,52.99999940395355",
			"sourceSymbol": "56a945c7-1231-4918-ae37-772f50348aab",
			"targetSymbol": "bb21ef78-e447-416c-b1ce-c87afc87c343",
			"object": "66866316-d68d-4714-ab3e-4bae1484fb15"
		},
		"bb21ef78-e447-416c-b1ce-c87afc87c343": {
			"classDefinition": "com.sap.bpm.wfs.ui.UserTaskSymbol",
			"x": 799.9999952316284,
			"y": 17.999999403953552,
			"width": 100,
			"height": 60,
			"object": "556699ef-229f-4a73-8149-76af5e6bbec0",
			"symbols": {
				"15274aae-fa68-4157-9ff9-91d69761c11a": {}
			}
		},
		"d0cf69c1-b5e4-4192-a880-86f71f80aaf5": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "849.9999952316284,47.99999940395355 988.9999976158142,48",
			"sourceSymbol": "bb21ef78-e447-416c-b1ce-c87afc87c343",
			"targetSymbol": "7b5e8f03-9e60-473c-8817-298bfe837aae",
			"object": "d165c590-3658-45d0-bdeb-53cb5da0fde7"
		},
		"3030c441-3a52-4207-b6cf-142fa6b380a6": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "1191.9999940395355,138.24999910593033 1290,138.25 1290,254.25",
			"sourceSymbol": "933de791-e8bc-404c-8a29-93a74d35fea2",
			"targetSymbol": "e04cb057-e5a1-44ec-8072-f256bebc46e1",
			"object": "533a7b5a-d2e2-495d-86b8-12d93f25ac9c"
		},
		"41d86f94-7e2f-4af3-b092-f87c14900b53": {
			"classDefinition": "com.sap.bpm.wfs.ui.MailTaskSymbol",
			"x": 599.9999976158142,
			"y": 263.9999988079071,
			"width": 100,
			"height": 60,
			"object": "5d04e883-64e6-43f0-bf85-0553f6e9c8ec"
		},
		"2c427009-acf1-45a9-a759-818f3f9632ac": {
			"classDefinition": "com.sap.bpm.wfs.ui.MailTaskSymbol",
			"x": 799.9999940395355,
			"y": -121,
			"width": 100,
			"height": 60,
			"object": "e7b22343-9350-40db-bbec-6d251a89b2a7"
		},
		"dc5937d2-5736-4a47-b07c-708b773cfe25": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "690,276 690,226.75 651,226.75 650.9999964237213,189.99999910593033",
			"sourceSymbol": "41d86f94-7e2f-4af3-b092-f87c14900b53",
			"targetSymbol": "4526e92a-7e2a-4857-b4cb-3a6e20e52e0b",
			"object": "d7cacca6-c3c7-4879-b67b-5bd8939ce5c4"
		},
		"e03d5578-aa2b-492e-b335-dd30e4c84ff5": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "834.999994635582,-75 834.999994635582,17.999999403953552",
			"sourceSymbol": "2c427009-acf1-45a9-a759-818f3f9632ac",
			"targetSymbol": "bb21ef78-e447-416c-b1ce-c87afc87c343",
			"object": "5bd266bf-e39a-4d72-bbde-2cd056adcf7b"
		},
		"b829c315-430e-415a-a4da-d156acaebb43": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "890.9999952316284,1.9999994039535522 891,-29.5 862,-29.5 861.9999940395355,-76",
			"sourceSymbol": "15274aae-fa68-4157-9ff9-91d69761c11a",
			"targetSymbol": "2c427009-acf1-45a9-a759-818f3f9632ac",
			"object": "cd354ce3-b5fc-4ce3-9d19-1c6e726904af"
		},
		"d46048dc-6ac8-4fce-8eb1-dcf36acd96d1": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "560.8014810070019,205.9867425699847 560.8014526367188,234.99337768554688 617,234.99337768554688 617,265",
			"sourceSymbol": "de005a46-4df5-4a5a-ab29-06c8241dd175",
			"targetSymbol": "41d86f94-7e2f-4af3-b092-f87c14900b53",
			"object": "42fde8cb-f41b-4a73-a1bf-b470fce59ae9"
		},
		"00fdaf34-0bd4-46b1-9799-ff79d96c8dc7": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "313.9999988079071,63.99999940395355 314,30 421.4999964237213,30.000000298023224",
			"sourceSymbol": "4ef6f2c7-95f5-44d5-8f2a-cb36b8934ba6",
			"targetSymbol": "56a945c7-1231-4918-ae37-772f50348aab",
			"object": "100ba38c-9bde-4ec0-9f8f-f5a357add8a3"
		},
		"8f3c62c3-c1aa-4183-9f07-c47fc89f7caf": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "988.9999976158142,48 1171,48 1171,139",
			"sourceSymbol": "7b5e8f03-9e60-473c-8817-298bfe837aae",
			"targetSymbol": "933de791-e8bc-404c-8a29-93a74d35fea2",
			"object": "1459b707-c32d-48d6-952b-18bc23bb6fde"
		},
		"0df81231-921d-4023-a050-09aca5902f75": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 850,
			"y": 152,
			"object": "436e1a00-db01-4b9b-beae-5a3fec66f632"
		},
		"afdce8da-5b11-4c92-846b-a34fbefdafa1": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "871,173 871,253.5 1272.4999928474426,253.49999940395355",
			"sourceSymbol": "0df81231-921d-4023-a050-09aca5902f75",
			"targetSymbol": "e04cb057-e5a1-44ec-8072-f256bebc46e1",
			"object": "f2f3e0ec-e8b1-4278-be78-412792743b9d"
		},
		"a9b5c216-a7c8-4583-8c19-85b6d57be44c": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "607.9999964237213,159.99999910593033 754.25,160 754.25,173 871,173",
			"sourceSymbol": "4526e92a-7e2a-4857-b4cb-3a6e20e52e0b",
			"targetSymbol": "0df81231-921d-4023-a050-09aca5902f75",
			"object": "55de8f0f-415d-4ed3-81c0-adb99762554e"
		},
		"44c40ec9-f716-4337-b542-d6946f1ce74b": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 321,
			"y": 206,
			"width": 100,
			"height": 60,
			"object": "9168f39c-ddba-4784-bf4a-38e865fedd4d"
		},
		"b9cc4d7f-dbfb-4274-842f-828fcb4a1db9": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "371,236 489.75,236 489.75,160 607.9999964237213,159.99999910593033",
			"sourceSymbol": "44c40ec9-f716-4337-b542-d6946f1ce74b",
			"targetSymbol": "4526e92a-7e2a-4857-b4cb-3a6e20e52e0b",
			"object": "9be491f4-ae62-43eb-93ea-4c1a8996faca"
		},
		"44050e9c-0b12-4e2e-a57f-02fe652bac32": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "45,151.49999940395355 218,151.49999940395355",
			"sourceSymbol": "1d602b09-19c8-4ff0-bc04-0a30c75784fc",
			"targetSymbol": "2c2b5b29-e733-40db-b919-521c99d2c798",
			"object": "30a83deb-29e0-40cc-9486-c15b0ba6f417"
		},
		"94512af6-318a-42cd-a592-28bf38431c7b": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "871,173 934.5,173 934.5,155 1006,155",
			"sourceSymbol": "0df81231-921d-4023-a050-09aca5902f75",
			"targetSymbol": "264a01c9-8168-4a5d-8e75-0e012952955a",
			"object": "9b721ade-6c63-43e7-bbe3-db6bf29fc42f"
		},
		"d7e046d0-83fb-4fb7-a88f-8abf6288cf8f": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "1026.9999976158142,168.9999988079071 1113.75,169 1113.75,139 1161,139",
			"sourceSymbol": "264a01c9-8168-4a5d-8e75-0e012952955a",
			"targetSymbol": "933de791-e8bc-404c-8a29-93a74d35fea2",
			"object": "2c7bdbd0-9874-42f1-9f55-78cbdfa530db"
		},
		"6b1f7d34-bac3-4650-9e70-ac3928fe9a25": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "230,151.49999940395355 230,404.5 570.9999988079071,404.49999940395355",
			"sourceSymbol": "2c2b5b29-e733-40db-b919-521c99d2c798",
			"targetSymbol": "f5a25c1d-2539-4781-99b3-9f990f55494f",
			"object": "2100776b-6bf6-4930-8ffc-4ee3018669ab"
		},
		"ec632b9c-b6fd-4cd3-bf70-a5e3582f77c9": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "570.9999988079071,404.49999940395355 1290,404.5 1290,251",
			"sourceSymbol": "f5a25c1d-2539-4781-99b3-9f990f55494f",
			"targetSymbol": "e04cb057-e5a1-44ec-8072-f256bebc46e1",
			"object": "059bd362-3a1c-43c5-bda1-d6c2d303904a"
		},
		"de005a46-4df5-4a5a-ab29-06c8241dd175": {
			"classDefinition": "com.sap.bpm.wfs.ui.BoundaryEventSymbol",
			"x": 544.8014810070019,
			"y": 173.9867425699847,
			"object": "037127a8-ab1b-4f54-a2ad-0927c1be0981"
		},
		"15274aae-fa68-4157-9ff9-91d69761c11a": {
			"classDefinition": "com.sap.bpm.wfs.ui.BoundaryEventSymbol",
			"x": 874.9999952316284,
			"y": 1.9999994039535522,
			"object": "0974929d-1782-4761-ada4-ee6ba312e9be"
		},
		"689b8833-adb9-45d6-93be-a618abdc28d4": {
			"classDefinition": "com.sap.bpm.wfs.LastIDs",
			"timereventdefinition": 5,
			"maildefinition": 2,
			"sequenceflow": 44,
			"startevent": 1,
			"intermediatetimerevent": 2,
			"boundarytimerevent": 2,
			"endevent": 1,
			"usertask": 2,
			"servicetask": 4,
			"scripttask": 2,
			"mailtask": 2,
			"exclusivegateway": 5
		},
		"f410760b-87fb-42bd-8936-b5f27924d154": {
			"classDefinition": "com.sap.bpm.wfs.MailDefinition",
			"name": "maildefinition1",
			"to": "${context.emailId}",
			"subject": "Pending Action for Change Business Partner",
			"reference": "/webcontent/Manage_BP/email_UpdateBP.html",
			"id": "maildefinition1"
		},
		"5bd67148-54ff-414e-b276-364533f433dd": {
			"classDefinition": "com.sap.bpm.wfs.MailDefinition",
			"name": "maildefinition2",
			"to": "${context.emailId}",
			"subject": "Pending Action for Create Business Partner",
			"reference": "/webcontent/Manage_BP/email_createBP.html",
			"id": "maildefinition2"
		}
	}
}